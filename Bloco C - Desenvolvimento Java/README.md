# Projeto Me Ajuda Francisco

## Mini-TCC 

### Universidade: INFNET

### Curso: Engenharia da Computação com Enfase em Software.

### Turma:  Engenharia da Computação 6 (EDC-6)

### Bloco C: Desenvolvimento Java

### Autora: Raysa Dutra

### Data: 14 Out 2016

### Linguagem: Java com Spring MVC

### Objetivo: Ajudar os animais abandonados ou possivelmente feridos, animais resgatados acharam um lar, listar instituições que ajudam esses animais que necessitam de doação e ajudar os protetores ou qualquer pessoa que deseja ajudar um animal. Protetores de animais, pessoas que desejam ajudar um animal ou alguma intuição e adotar um animal.

#### Dedico aos meus amigos, a minha família ao meu cachorro Jorge e ao meu futuro marido ruivo.

##### O aplicativo está dentro da pasta "ProjetoMeAjudaFrancisco", a documentação na pasta "Documentação", o arquivo "Projeto.pdf", foi usado para apresentar esse trabalho e mostrar a documentação feita durante o bloco.

--------------------

## Term Paper

### Collage: INFNET

### Course: Computer Engineering with Emphasis in Software.

### Class: Computer Engineering 6 (EDC-6)

### Block C: Java Development

### Author: Raysa Dutra

### Date: 14 Oct 2016

### Language: Java with Spring MVC

### Objective: Helping abandoned or possibly injured animals, rescued animals find a home, listing institutions that help those animals that need donation and help the protectors or anyone who wants to help an animal. Protectors of animals, people who wish to help an animal or some intuition and adopt an animal.

#### I dedicate to my friends, my family, my dog Jorge and my future husband.

##### The application is inside the "ProjetoMeAjudaFrancisco" folder, the documentation is in the "Documentação" folder, the "Projeto.pdf" file, was used to present this work and show the documentation made during the block.
