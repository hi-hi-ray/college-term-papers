# Travel to Rio

## Mini-TCC 

### Universidade: INFNET

### Curso: Engenharia da Computação com Enfase em Software.

### Turma:  Engenharia da Computação 6 (EDC-6)

### Bloco B: Computação Básica 

### Autora: Raysa Dutra & Pedro Bastos

### Data: 19 Abr 2016

### Linguagem: Swfit / Objective-C

### Objetivo: Mapear os lugares que tem maior indice de criminalidade para que os turistas que viessem para o Rio de Janeiro, elas tomassem cuidado e tivessem um guia de ajuda caso algo de ruim acontecesse.

#### Dedico aos meus amigos, a minha família ao meu cachorro Jorge e ao meu futuro marido ruivo.

##### O aplicativo está dentro da pasta "app", e a documentação no arquivo power point "Projeto.pptx", ele foi usado para apresentar esse trabalho e mostrar a documentação feita durante o bloco.

--------------------

## Term Parper

### Collage: INFNET

### Course: Computer Engineering with Emphasis in Software.

### Class: Computer Engineering 6 (EDC-6)

### Block B: Basic Computing

### Author: Raysa Dutra & Pedro Bastos

### Date: 19 Apr 2016

### Language: Swfit / Objective-C

### Objective: Map the places that have the highest crime rate, so that tourists which is coming to Rio de Janeiro, take care and have a helper guide if something bad happenes.

#### I dedicate to my friends, my family, my dog Jorge and my future husband.

##### The application is inside the "app" folder, and the documentation in the power point file "Projeto.pptx", it was used to present this work and show the documentation made during the block.