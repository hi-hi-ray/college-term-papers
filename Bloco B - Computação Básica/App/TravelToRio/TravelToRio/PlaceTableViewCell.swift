//
//  PlaceTableViewCell.swift
//  TravelToRio
//
//  Created by Raysa Dutra on 01/04/16.
//  Copyright © 2016 Raysa Dutra. All rights reserved.
//

import UIKit

class PlaceTableViewCell: UITableViewCell {

    @IBOutlet weak var lbplace: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
