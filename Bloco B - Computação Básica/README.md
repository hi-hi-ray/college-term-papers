# Visita Rio

## Mini-TCC 

### Universidade: INFNET

### Curso: Engenharia da Computação com Enfase em Software.

### Turma:  Engenharia da Computação 6 (EDC-6)

### Bloco A: Front-End 

### Autora: Raysa Dutra

### Data: 7 Out 2015

### Linguagem: Cordova / PhoneGap com HTML, CSS and Javascript / JQuery

### Objetivo: Ajudar turistas a se guiarem no Rio de Janeiro.

#### Dedico aos meus amigos, a minha família ao meu cachorro Jorge e ao meu futuro marido ruivo.

##### O aplicativo está dentro da pasta "app", e a documentação no arquivo power point "Projeto.pptx", ele foi usado para apresentar esse trabalho e mostrar a documentação feita durante o bloco.

--------------------

## Term Paper

### Collage: INFNET

### Course: Computer Engineering with Emphasis in Software.

### Class: Computer Engineering 6 (EDC-6)

### Block A: Front-End 

### Author: Raysa Dutra

### Date: 7 Oct 2015

### Language: Cordova / PhoneGap with HTML, CSS and Javascript / JQuery

### Objective: To help tourists to guide themselves in Rio de Janeiro.

#### I dedicate to my friends, my family, my dog Jorge and my future husband.

##### The application is inside the "app" folder, and the documentation in the power point file "Projeto.pptx", it was used to present this work and show the documentation made during the block.
