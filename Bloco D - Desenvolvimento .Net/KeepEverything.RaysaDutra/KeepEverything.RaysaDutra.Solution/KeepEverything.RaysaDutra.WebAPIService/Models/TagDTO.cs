﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KeepEverything.RaysaDutra.WebAPIService.Models
{
    public class TagDTO
    {
        public int IdTag { get; set; }
        public string NameTag { get; set; }
    }
}