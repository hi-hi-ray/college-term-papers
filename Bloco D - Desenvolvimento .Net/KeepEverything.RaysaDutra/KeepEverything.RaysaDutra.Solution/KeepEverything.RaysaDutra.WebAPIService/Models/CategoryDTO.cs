﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KeepEverything.RaysaDutra.WebAPIService.Models
{
    public class CategoryDTO
    {
        public int IdCategory { get; set; }
        public string NameCategory { get; set; }
        public string DescriptionCategory { get; set; }
    }
}