﻿using System;
using System.Collections.Generic;


namespace KeepEverything.RaysaDutra.Presentation.Models
{
    public class CategoryViewModel
    {
        public int IdCategory { get; set; }
        public string NameCategory { get; set; }
        public string DescriptionCategory { get; set; }
        //public virtual ICollection<TaskViewModel> Tasks { get; set; }

    }
}