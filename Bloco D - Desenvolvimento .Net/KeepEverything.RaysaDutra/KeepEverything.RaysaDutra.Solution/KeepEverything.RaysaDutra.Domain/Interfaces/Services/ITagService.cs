﻿using KeepEverything.RaysaDutra.Domain.Entities;

namespace KeepEverything.RaysaDutra.Domain.Interfaces.Services
{
    public interface ITagService : IServiceBase<Tag>
    {
    }
}
