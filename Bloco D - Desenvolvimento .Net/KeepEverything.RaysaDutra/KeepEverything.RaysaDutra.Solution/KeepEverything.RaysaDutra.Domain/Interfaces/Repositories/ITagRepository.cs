﻿using KeepEverything.RaysaDutra.Domain.Entities;

namespace KeepEverything.RaysaDutra.Domain.Interfaces.Repositories
{
    public interface ITagRepository : IRepositoryBase<Tag>
    {
    }
}
