﻿namespace KeepEverything.RaysaDutra.SecurityIdentity.Models
{
    public class FactorViewModel
    {
        public string Purpose { get; set; }
    }
}