﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KeepEverything.Consumer.RaysaDutra.Models
{
    public class CategoryViewModel
    {
        public int IdCategory { get; set; }
        public string NameCategory { get; set; }
        public string DescriptionCategory { get; set; }
    }
}