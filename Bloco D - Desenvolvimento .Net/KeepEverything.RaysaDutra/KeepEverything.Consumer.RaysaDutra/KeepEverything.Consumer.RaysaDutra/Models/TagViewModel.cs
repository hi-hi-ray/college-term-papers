﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KeepEverything.Consumer.RaysaDutra.Models
{
    public class TagViewModel
    {
        public int IdTag { get; set; }
        public string NameTag { get; set; }
    }
}