# Projeto Keep Everything

## Mini-TCC 

### Universidade: INFNET

### Curso: Engenharia da Computação com Enfase em Software.

### Turma:  Engenharia da Computação 6 (EDC-6)

### Bloco D: Desenvolvimento .Net

### Autora: Raysa Dutra

### Data: 17 Out 2017

### Linguagem: .Net com MVC/WebAPI

### Objetivo: Ajudar o usuário a se organizar, podendo gerenciar suas tarefas, as classificando, determinando um prazo para ela ser concluída e por final adicionar uma descrição para que ele saiba melhor o que ele tenha que ser feito. 

#### Dedico aos meus amigos, a minha família ao meu cachorro Jorge e ao meu futuro marido ruivo.

##### O aplicativo está dentro da pasta "KeepEverything.RaysaDutra", a documentação na pasta "Documentação", o arquivo "Projeto.pdf", foi usado para apresentar esse trabalho e mostrar a documentação feita durante o bloco.

###### Links

- Web site para o Sistema de Tarefas (ASP.NET MVC).
http://keepeverythingraysadutra.azurewebsites.net/
- Web site para o Web Service (ASP.NET Web API).
http://keepeverythingraysadutrawebapi.azurewebsites.net/
- Web site para o Consumer (ASP.NET Web API/ MVC).
http://keepeverythingraysadutraconsumer.azurewebsites.net/

--------------------

## Term Paper

### Collage: INFNET

### Course: Computer Engineering with Emphasis in Software.

### Class: Computer Engineering 6 (EDC-6)

### Block D: .Net Development

### Author: Raysa Dutra

### Date: 17 Apr 2017

### Language: .Net with MVC/WebAPI

### Objective: Help the user to organize, being able to manage their tasks, classifying them, determining a deadline for it to be completed and finally adding a description so that it knows better what it has to be done.

#### I dedicate to my friends, my family, my dog Jorge and my future husband.

##### The application is inside the "KeepEverything.RaysaDutra" folder, the documentation is in the "Documentação" folder, the "Projeto.pdf" file, was used to present this work and show the documentation made during the block.

###### Links

- Website for the System of Tasks (ASP.NET MVC).
http://keepeverythingraysadutra.azurewebsites.net/
- Website for the Web Service (ASP.NET Web API).
http://keepeverythingraysadutrawebapi.azurewebsites.net/
- Website for the Consumer (ASP.NET Web API/ MVC).
http://keepeverythingraysadutraconsumer.azurewebsites.net/
