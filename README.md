# College Term Papers :mortar_board:

## Esse repositório tem como função guardar os mini-tcc's que a minha universidade exige a cada bloco.

### Lista de Blocos já Concluídos:

* [Bloco A: Front-End](https://github.com/hi-hi-ray/college-term-papers/tree/master/Bloco%20A%20-%20Front-End) :star2:
* [Bloco B: Computação Básica](https://github.com/hi-hi-ray/college-term-papers/tree/master/Bloco%20B%20-%20Computa%C3%A7%C3%A3o%20B%C3%A1sica) :star2:
* [Bloco C: Desenvolvimento Java](https://github.com/hi-hi-ray/college-term-papers/tree/master/Bloco%20C%20-%20Desenvolvimento%20Java) :star2:
* [Bloco D: Desenvolvimento .Net](https://github.com/hi-hi-ray/college-term-papers/tree/master/Bloco%20D%20-%20Desenvolvimento%20.Net) :star2:

----------

## This repository has the function of saving the term papers that my university requires for each block.

### List of Blocks Already Completed:

* [Block A: Front-End](https://github.com/hi-hi-ray/college-term-papers/tree/master/Bloco%20A%20-%20Front-End) :star2:
* [Block B: Basic Computing](https://github.com/hi-hi-ray/college-term-papers/tree/master/Bloco%20B%20-%20Computa%C3%A7%C3%A3o%20B%C3%A1sica) :star2:
* [Block C: Java Development](https://github.com/hi-hi-ray/college-term-papers/tree/master/Bloco%20C%20-%20Desenvolvimento%20Java) :star2:
* [Block D: .Net Development](https://github.com/hi-hi-ray/college-term-papers/tree/master/Bloco%20D%20-%20Desenvolvimento%20.Net) :star2:

